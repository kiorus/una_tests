package ph.com.unabank.tests.dictionaries.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import ph.com.unabank.tests.dictionaries.DictionariesEndPoints;
import ph.com.unabank.tests.dictionaries.controllers.*;
import ph.com.unabank.tests.dictionaries.models.*;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;

@Execution(ExecutionMode.CONCURRENT)
public class DictionaryPositiveTests {

    @Test
    public void publicValidate() throws JsonProcessingException {

        var listItems = new ArrayList<PublicValidateRequestListItems>();
        listItems.add(PublicValidateRequestListItems.builder().dictionary("proof_of_income").value("BUSINESS_PERMIT").build());
        listItems.add(PublicValidateRequestListItems.builder().dictionary("proof_of_income1").value("BUSINESS_PERMIT").build());

        var publicValidateModel = PublicValidateModel.builder().list(listItems).build();

        var objectMapper = new ObjectMapper();
        var json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(publicValidateModel);

        var controller = new PublicValidateController();
        var response = controller.doPostRequest(json);

        assertThat(response.jsonPath().get("list.valid[0]"), Matchers.equalTo(true));
        assertThat(response.jsonPath().get("list.valid[1]"), Matchers.equalTo(false));
    }

    @Test
    public void document_validationData() {

        var controller = new CommonController(DictionariesEndPoints.DOCUMENT_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(DocumentResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(DocumentResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("PASSPORT","DRIVER_LICENCE"));
        for (DocumentListItems listItems : response.as(DocumentResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("PASSPORT"):
                    assertThat(listItems.getMask(), Matchers.equalTo("[A0000000A]"));
                    break;
                case ("DRIVER_LICENCE"):
                    assertThat(listItems.getMask(), Matchers.equalTo("[A00]-[00]-[000000]"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void emergencyContactType_validationData() {

        var controller = new CommonController(DictionariesEndPoints.EMERGENCY_CONTACT_TYPE_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(EmergencyContactTypeResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(EmergencyContactTypeResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("BROTHER_SISTER","WIFE_HUSBAND"));

        for (CommonListItems listItems : response.as(EmergencyContactTypeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BROTHER_SISTER"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Brother / Sister"));
                    break;
                case ("WIFE_HUSBAND"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Wife / Husband"));
                    break;
                default:
                    break;
            }
        }
     }

    @Test
    public void proofOfBilling_validationData() {

        var controller = new CommonController(DictionariesEndPoints.PROOF_OF_BILLING_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(ProofOfBillingResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(ProofOfBillingResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("CREDIT_CARD_BILL","WATER_BILL"));

        for (CommonListItems listItems : response.as(ProofOfBillingResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("CREDIT_CARD_BILL"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Credit Card Bill"));
                    break;
                case ("WATER_BILL"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Water Bill"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void soloServiceType_validationData() {

        var controller = new CommonController(DictionariesEndPoints.SOLO_SERVICE_TYPE_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(SoloServiceTypeResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(SoloServiceTypeResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("MAINTENANCE","IT_FREELANCE"));

        for (CommonListItems listItems : response.as(SoloServiceTypeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("MAINTENANCE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Maintenance"));
                    break;
                case ("IT_FREELANCE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("IT freelance"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void workingIndustry_validationData() {

        var controller = new CommonController(DictionariesEndPoints.WORKING_INDUSTRY_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",21);
        var response = controller.doGetRequest();

        assertThat(response.as(WorkingIndustryResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(WorkingIndustryResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("ENGINEERING","GOVERNMENT_EMPLOYEE","MEDIA","FINANCE"));

        for (WorkingIndustryListItems listItems : response.as(WorkingIndustryResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("ENGINEERING"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Engineering"));
                    break;
                case ("GOVERNMENT_EMPLOYEE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Government Employee"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void smallBusinessType_validationData() {

        var controller = new CommonController(DictionariesEndPoints.SMALL_BUSINESS_TYPE_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(SmallBusinessTypeResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(SmallBusinessTypeResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("BARBER_SHOP","VULCANIZING_SHOP"));

        for (CommonListItems listItems : response.as(SmallBusinessTypeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BARBER_SHOP"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Parlor / Salor / Barber shop"));
                    break;
                case ("VULCANIZING_SHOP"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Vulcanizing shop"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void incomeBasis_validationData() {

        var controller = new CommonController(DictionariesEndPoints.INCOME_BASIS);
        var response = controller.doGetRequest();

        assertThat(response.as(IncomeBasisResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(IncomeBasisResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("BI_MONTHLY","DAILY","MONTHLY","RANDOM","WEEKLY"));

        for (CommonListItems listItems : response.as(IncomeBasisResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BI_MONTHLY"):
                    assertThat(listItems.getValue(), Matchers.equalTo("bi-monthly"));
                    break;
                case ("RANDOM"):
                    assertThat(listItems.getValue(), Matchers.equalTo("random"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void geoRegion_validationData() {

        var controller = new CommonController(DictionariesEndPoints.GEO_REGION_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",17);
        var response = controller.doGetRequest();

        assertThat(response.as(GeoRegionResponseModel.class).getList().size(),
                Matchers.equalTo(response.as(GeoRegionResponseModel.class).getTotalElements()));

        response.then().body("list.id", hasItems("50000","89794"));

        for (GeoRegionResponseListItems listItems : response.as(GeoRegionResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("50000"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(1));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("10000000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Region I (Ilocos Region)"));
                    break;
                case ("89794"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(1));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("150000000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Autonomous Region In Muslim Mindanao (ARMM)"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void geoProvince_validationData() {

        var controller = new CommonController(DictionariesEndPoints.GEO_PROVINCE_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",20);
        var response = controller.doGetRequest();

        assertThat(response.as(GeoProvinceResponseModel.class).getTotalPages(), Matchers.equalTo(5));
        assertThat(response.as(GeoProvinceResponseModel.class).getTotalElements(), Matchers.equalTo(82));

        response.then().body("list.id", hasItems("50001","61726"));

        for (GeoProvinceResponseListItems listItems : response.as(GeoProvinceResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("50001"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(2));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("12800000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Ilocos Norte"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("50000"));
                    break;
                case ("61726"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(2));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("45600000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Quezon"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("59047"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void geoCity_validationData() {

        var controller = new CommonController(DictionariesEndPoints.GEO_CITY_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",20);
        var response = controller.doGetRequest();

        assertThat(response.as(GeoCityResponseModel.class).getTotalPages(), Matchers.equalTo(82));
        assertThat(response.as(GeoCityResponseModel.class).getTotalElements(), Matchers.equalTo(1634));

        response.then().body("list.id", hasItems("50002","50477"));

        for (GeoCityResponseListItems listItems : response.as(GeoCityResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("50002"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(3));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("12801000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Adams"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("50001"));
                    break;
                case ("50477"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(3));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("12820000"));
                    assertThat(listItems.getValue(), Matchers.equalTo("San Nicolas"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("50001"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void geoBarangay_validationData() {

        var controller = new CommonController(DictionariesEndPoints.GEO_BARANGAY_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",20);
        var response = controller.doGetRequest();

        assertThat(response.as(GeoBarangayResponseModel.class).getTotalPages(), Matchers.equalTo(2103));
        assertThat(response.as(GeoBarangayResponseModel.class).getTotalElements(), Matchers.equalTo(42046));

        response.then().body("list.id", hasItems("50003","50023"));

        for (GeoBarangayResponseListItems listItems : response.as(GeoBarangayResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("50003"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(4));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("12801001"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Adams (Pob.)"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("50002"));
                    break;
                case ("50023"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(4));
                    assertThat(listItems.getPsgcCode(), Matchers.equalTo("12802020"));
                    assertThat(listItems.getValue(), Matchers.equalTo("Pipias"));
                    assertThat(listItems.getParentId(), Matchers.equalTo("50004"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void geoCountry_validationData() {

        var controller = new CommonController(DictionariesEndPoints.GEO_COUNTRY_SEARCH);
        controller.getRequestSpecBuilder().addParam("size",20);
        var response = controller.doGetRequest();

        assertThat(response.as(GeoCountryResponseModel.class).getTotalPages(), Matchers.equalTo(10));
        assertThat(response.as(GeoCountryResponseModel.class).getTotalElements(), Matchers.equalTo(195));

        response.then().body("list.id", hasItems("100000","100019"));

        for (GeoCountryResponseListItems listItems : response.as(GeoCountryResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("100000"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(5));
                    assertThat(listItems.getValue(), Matchers.equalTo("Philippines"));
                    break;
                case ("100019"):
                    assertThat(listItems.getLevel(), Matchers.equalTo(5));
                    assertThat(listItems.getValue(), Matchers.equalTo("Benin"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void sourceOfFunds_validationData() {

        var controller = new CommonController(DictionariesEndPoints.SOURCE_OF_FUNDS_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(SourceOfFundsResponseModel.class).getTotalElements(), Matchers.equalTo(5));

        response.then().body("list.id", hasItems("BUSINESS_INCOME","UNEMPLOYED"));

        for (CommonListItems listItems : response.as(SourceOfFundsResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BUSINESS_INCOME"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Business Income"));
                    break;
                case ("UNEMPLOYED"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Unemployed"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void onlineSellerType_validationData() {

        var controller = new CommonController(DictionariesEndPoints.ONLINE_SELLER_TYPE_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(OnlineSellerTypeResponseModel.class).getTotalElements(), Matchers.equalTo(5));

        response.then().body("list.id", hasItems("CAROUSELL_STORE","OTHER_STORE"));

        for (CommonListItems listItems : response.as(OnlineSellerTypeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("CAROUSELL_STORE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Carousell store"));
                    break;
                case ("OTHER_STORE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Other store"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void driverType_validationData() {

        var controller = new CommonController(DictionariesEndPoints.DRIVER_TYPE_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(DriverTypeResponseModel.class).getTotalElements(), Matchers.equalTo(5));

        response.then().body("list.id", hasItems("BIKE","TRICYCLE_RIDER"));

        for (CommonListItems listItems : response.as(DriverTypeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BIKE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Bike / MC taxi"));
                    break;
                case ("TRICYCLE_RIDER"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Tricycle rider"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void officemateContact_validationData() {

        var controller = new CommonController(DictionariesEndPoints.OFFICEMATE_CONTACT_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(OfficemateContactResponseModel.class).getTotalElements(), Matchers.equalTo(3));

        response.then().body("list.id", hasItems("BOSS","SUBORDINATE"));

        for (CommonListItems listItems : response.as(OfficemateContactResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BOSS"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Boss"));
                    break;
                case ("SUBORDINATE"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Subordinate"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void proofOfIncome_validationData() {

        var controller = new CommonController(DictionariesEndPoints.PROOF_OF_INCOME_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(ProofOfIncomeResponseModel.class).getTotalElements(), Matchers.equalTo(3));

        response.then().body("list.id", hasItems("BUSINESS_PERMIT","LATEST_PAYSLIP"));

        for (CommonListItems listItems : response.as(ProofOfIncomeResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BUSINESS_PERMIT"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Business permit"));
                    break;
                case ("LATEST_PAYSLIP"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Latest Payslip"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void typeOfWork_validationData() {

        var controller = new CommonController(DictionariesEndPoints.TYPE_OF_WORK_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(TypeOfWorkResponseModel.class).getTotalElements(), Matchers.equalTo(13));

        response.then().body("list.id", hasItems("BPO_SERVICES","UNEMPLOYMENT"));

        for (CommonListItems listItems : response.as(TypeOfWorkResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("BPO_SERVICES"):
                    assertThat(listItems.getValue(), Matchers.equalTo("BPO services"));
                    break;
                case ("UNEMPLOYMENT"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Unemployment"));
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void citizenship_validationData() {

        var controller = new CommonController(DictionariesEndPoints.CITIZENSHIP_SEARCH);
        var response = controller.doGetRequest();

        assertThat(response.as(CitizenshipResponseModel.class).getTotalElements(), Matchers.equalTo(249));
        assertThat(response.as(CitizenshipResponseModel.class).getTotalPages(), Matchers.equalTo(13));

        response.then().body("list.id", hasItems("PH","BD"));

        for (CommonListItems listItems : response.as(CitizenshipResponseModel.class).getList()) {
            switch (listItems.getId()) {
                case ("PH"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Philippines"));
                    break;
                case ("BD"):
                    assertThat(listItems.getValue(), Matchers.equalTo("Bangladesh"));
                    break;
                default:
                    break;
            }
        }
    }

}
