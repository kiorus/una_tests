package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class WorkingIndustryListItems {
	private String id;
	private String value;
	@JsonProperty("is_actual")
	private boolean isActual;
}