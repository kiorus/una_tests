package ph.com.unabank.tests.dictionaries;

public class DictionariesEndPoints {

    public static final String DOCUMENT_SEARCH = "/document/search";
    public static final String EMERGENCY_CONTACT_TYPE_SEARCH = "/emergency_contact_type/search";
    public static final String PROOF_OF_BILLING_SEARCH = "/proof_of_billing/search";
    public static final String PUBLIC_VALIDATE = "/validate";
    public static final String GENERAL_ALL = "/general/all";
    public static final String SOLO_SERVICE_TYPE_SEARCH = "/solo_service_type/search";
    public static final String WORKING_INDUSTRY_SEARCH = "/working_industry/search";
    public static final String SMALL_BUSINESS_TYPE_SEARCH = "/small_business_type/search";
    public static final String INCOME_BASIS = "/income_basis/search";
    public static final String GEO_COUNTRY_SEARCH = "/geo/country/search";
    public static final String GEO_PROVINCE_SEARCH = "/geo/province/search";
    public static final String GEO_CITY_SEARCH = "/geo/city/search";
    public static final String GEO_BARANGAY_SEARCH = "/geo/barangay/search";
    public static final String GEO_REGION_SEARCH = "/geo/region/search";
    public static final String SOURCE_OF_FUNDS_SEARCH = "/source_of_funds/search";
    public static final String ONLINE_SELLER_TYPE_SEARCH = "/online_seller_type/search";
    public static final String DRIVER_TYPE_SEARCH = "/driver_type/search";
    public static final String OFFICEMATE_CONTACT_SEARCH = "/officemate_contact/search";
    public static final String PROOF_OF_INCOME_SEARCH = "/proof_of_income/search";
    public static final String TYPE_OF_WORK_SEARCH = "/type_of_work/search";
    public static final String CITIZENSHIP_SEARCH = "/citizenship/search";

}
