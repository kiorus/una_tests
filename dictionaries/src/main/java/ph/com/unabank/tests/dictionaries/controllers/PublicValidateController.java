package ph.com.unabank.tests.dictionaries.controllers;


import io.restassured.response.Response;
import ph.com.unabank.tests.dictionaries.DictionariesEndPoints;

import static io.restassured.RestAssured.given;

public class PublicValidateController extends BaseController {

    public Response doPostRequest(String json) {
        return given()
                .spec(getRequestSpecBuilder().build())
                .body(json)
                .post(DictionariesEndPoints.PUBLIC_VALIDATE);
    }

}
