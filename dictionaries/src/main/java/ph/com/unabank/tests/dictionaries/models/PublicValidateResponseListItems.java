package ph.com.unabank.tests.dictionaries.models;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PublicValidateResponseListItems {
	private boolean valid;
	private String dictionary;
	private String value;
}