package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class WorkingIndustryResponseModel {
	@JsonProperty("total_elements")
	private int totalElements;
	@JsonProperty("total_pages")
	private int totalPages;
	private List<WorkingIndustryListItems> list;
}