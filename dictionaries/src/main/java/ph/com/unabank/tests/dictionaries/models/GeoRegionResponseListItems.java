package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GeoRegionResponseListItems {
	private int level;
	private String id;
	private String value;
	@JsonProperty("psgc_code")
	private String psgcCode;
}