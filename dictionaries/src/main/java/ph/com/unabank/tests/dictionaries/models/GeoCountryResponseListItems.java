package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GeoCountryResponseListItems {
	private int level;
	private String id;
	private String value;
}