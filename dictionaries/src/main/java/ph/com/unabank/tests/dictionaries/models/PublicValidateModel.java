package ph.com.unabank.tests.dictionaries.models;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PublicValidateModel {
	private List<PublicValidateRequestListItems> list;
}