package ph.com.unabank.tests.dictionaries.controllers;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import lombok.Getter;
import org.hamcrest.Matchers;

public class BaseController {

    @Getter
    protected RequestSpecBuilder requestSpecBuilder;

    protected BaseController() {
        requestSpecBuilder = new RequestSpecBuilder()
                .setBaseUri("http://localhost")
                .setBasePath("/dictionaries/api/v1/public")
                .setContentType(ContentType.JSON);

        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectResponseTime(Matchers.lessThan(15000L))
                .expectContentType(ContentType.JSON)
                .build();
    }

}
