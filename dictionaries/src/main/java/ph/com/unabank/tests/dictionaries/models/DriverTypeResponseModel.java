package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class DriverTypeResponseModel {
	@JsonProperty("total_elements")
	private int totalElements;
	@JsonProperty("total_pages")
	private int totalPages;
	private List<CommonListItems> list;
}