package ph.com.unabank.tests.dictionaries.models;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CommonListItems {
	private String id;
	private String value;
}