package ph.com.unabank.tests.dictionaries.controllers;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class CommonController extends BaseController {

    private final String dictionaryEndpoint;

    public CommonController(String dictionaryEndpoint) {
        this.dictionaryEndpoint = dictionaryEndpoint;
    }

    public Response doGetRequest() {
        return given()
                .spec(getRequestSpecBuilder().build())
                .get(dictionaryEndpoint);
    }

}
