package ph.com.unabank.tests.dictionaries.models;

import lombok.*;

@Builder
@Getter
@ToString
public class PublicValidateRequestListItems {
	private final String dictionary;
	private final String value;
}