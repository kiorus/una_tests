package ph.com.unabank.tests.dictionaries.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class DocumentResponseModel {
	@JsonProperty("total_elements")
	private int totalElements;
	@JsonProperty("total_pages")
	private int totalPages;
	private List<DocumentListItems> list;
}