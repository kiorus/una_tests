package ph.com.unabank.tests.dictionaries.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class DocumentListItems {
	@JsonProperty("aspect_ratio")
	private String aspectRatio;
	@JsonProperty("is_double_sided")
	private boolean isDoubleSided;
	@JsonProperty("is_expiration_date_required")
	private boolean isExpirationDateRequired;
	@JsonProperty("short_name")
	private String shortName;
	private String id;
	private String value;
	private String mask;
}