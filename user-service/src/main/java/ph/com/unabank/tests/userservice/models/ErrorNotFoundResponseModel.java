package ph.com.unabank.tests.userservice.models;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ErrorNotFoundResponseModel {
    private String message;
}
