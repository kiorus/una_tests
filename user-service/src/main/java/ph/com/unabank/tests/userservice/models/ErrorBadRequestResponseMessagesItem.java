package ph.com.unabank.tests.userservice.models;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ErrorBadRequestResponseMessagesItem {
	private String path;
	private String type;
	private String message;
}