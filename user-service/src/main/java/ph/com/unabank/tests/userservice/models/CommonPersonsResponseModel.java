package ph.com.unabank.tests.userservice.models;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CommonPersonsResponseModel{
	private String firstName;
	private String lastName;
	private boolean linkedWithPep;
	private String cognitoId;
	private String middleName;
	private boolean pep;
	private String id;
	private String cbsId;
	private String updatedAt;
}