package ph.com.unabank.tests.userservice.models;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class ErrorBadRequestResponseModel {
	private String code;
	private List<ErrorBadRequestResponseMessagesItem> messages;
	private String error;
}